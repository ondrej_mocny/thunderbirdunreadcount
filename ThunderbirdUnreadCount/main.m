//
//  main.m
//  ThunderbirdUnreadCount
//
//  Created by Ondra on 10/19/13.
//  Copyright (c) 2013 Ondra. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
