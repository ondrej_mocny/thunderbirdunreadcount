//
//  AppDelegate.m
//  ThunderbirdUnreadCount
//
//  Created by Ondra on 10/19/13.
//  Copyright (c) 2013 Ondra. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
}


-(void)awakeFromNib
{
    statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
    
    NSImage* icon = [NSImage imageNamed:@"icon.png"];
    [statusItem setImage:icon];
    
    [statusItem setHighlightMode:YES];
    
    [statusItem setTarget:self];
    [statusItem setAction:@selector(onStatusItemClicked:)];
    //[statusItem setMenu:statusMenu];
    
    NSTimer *timer = [NSTimer
                      scheduledTimerWithTimeInterval:0.2
                      target:self
                      selector:@selector(onTimer:)
                      userInfo:Nil
                      repeats:YES];
    [self onTimer:timer];
}


-(void)onStatusItemClicked:(id)sender
{
    //NSAppleScript *run = [[NSAppleScript alloc] initWithSource:@"tell application \"Thunderbird\" to activate"];
	//[run executeAndReturnError:nil];
    
    BOOL activated = NO;
    NSArray *apps = [NSRunningApplication runningApplicationsWithBundleIdentifier:@"org.mozilla.thunderbird"];
    if ([apps count] > 0) {
        NSRunningApplication *app = [apps objectAtIndex:0];
        if (![app isActive]) {
            [app activateWithOptions:NSApplicationActivateIgnoringOtherApps];
            activated = YES;
        }
    } else {
        NSAppleScript *run = [[NSAppleScript alloc] initWithSource:@"tell application \"Thunderbird\" to activate"];
        [run executeAndReturnError:nil];
    }
    
    if (!activated) {
        [statusItem popUpStatusItemMenu:statusMenu];
    }
}


-(void)setTitle:(NSString *)text
{
    NSAttributedString *title = [[NSAttributedString alloc]
        initWithString:text attributes:@{
            NSForegroundColorAttributeName: [NSColor whiteColor],
            NSFontAttributeName: [NSFont fontWithName:@"Arial" size:15]
        }
    ];
    [statusItem setAttributedTitle:title];
}


-(void)onTimer:(NSTimer *)timer
{
    NSString *count = [NSString stringWithContentsOfFile:@"/tmp/testUnread" encoding:NSUTF8StringEncoding error:nil];
    if (count) {
        count = [count stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    }
    if (!count || [count isEqualToString:@"0"]) {
        count = @"";
    }
    [self setTitle:count];
}

@end
