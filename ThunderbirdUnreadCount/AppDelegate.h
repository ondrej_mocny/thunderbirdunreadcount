//
//  AppDelegate.h
//  ThunderbirdUnreadCount
//
//  Created by Ondra on 10/19/13.
//  Copyright (c) 2013 Ondra. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>
{
    IBOutlet NSMenu* statusMenu;
    NSStatusItem* statusItem;
}

@property (assign) IBOutlet NSWindow *window;

@end
